package com.nogroup.gov.gis.vui.prez.modules.dwelling;

import com.nogroup.gov.gis.vui.VUI;
import com.nogroup.gov.gis.vui.data.daos.DwellingD;
import com.nogroup.gov.gis.vui.data.entities.Dwelling;
import com.nogroup.gov.gis.vui.prez.ccmp.flds.MultiLangNameField;
import com.nogroup.gov.gis.vui.prez.ccmp.flds.SelectPointComponent;
import com.nogroup.gov.gis.vui.prez.ccmp.flds.UploadField;
import com.nogroup.gov.gis.vui.prez.ccmp.wndws.CreateWindow;
import com.nogroup.gov.gis.vui.prez.home.OnCissorsClicked;
import com.nogroup.gov.gis.vui.prez.home.OnGeometryDrawnWindowAction;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.ValoTheme;

public class CreateDwellingUnitV extends CreateWindow implements CloseListener{
	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	private Dwelling entity ;
	private SelectPointComponent geometryField;
	private MultiLangNameField tfAddress;
	protected String drawnFeatureId;
	
	public CreateDwellingUnitV(VUI ui) {
		super(ui) ;
		addCloseListener(this);
	}
	
	@Override
	public void saveButtonClicked() {
		fill() ;
		new DwellingD().create(entity) ;
		CreateDwellingUnitV.this.close();
	}

	@Override
	public String caption() {
		return "Settings";
	}

	@Override
	public ComponentContainer content() {
		VerticalLayout vl = new VerticalLayout() ;
		vl.setWidth("100%");
		tfAddress = new MultiLangNameField("Address") ;
		tfAddress.setWidth("100%");
		tfAddress.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tfAddress);
		
		geometryField = new SelectPointComponent(getUi(), new OnGeometryDrawnWindowAction() {

			@Override
			public void before(String data) {
				getUi().mapV.removeFeatureFromLayer("SELCTION LAYER",drawnFeatureId) ;
				CreateDwellingUnitV.this.setVisible(false);
			}

			@Override
			public void after(String data) {
				drawnFeatureId = data ;
				CreateDwellingUnitV.this.setVisible(true);
			}
			
		},true);
		geometryField.setOnCissorsClicked(new OnCissorsClicked() {

			@Override
			public void action(String data) {
				getUi().mapV.removeFeatureFromLayer("SELCTION LAYER",drawnFeatureId) ;
			}
			
		});
		vl.addComponent(geometryField);
		
		vl.addComponent(new UploadField("Upload Permit"));
		vl.addComponent(new UploadField("Upload Permit2"));
		vl.addComponent(new UploadField("Upload Permit3"));
		return vl ;
	}

	@Override
	public void cancelButtonCallBack() {
		CreateDwellingUnitV.this.close();
	}
	
	public void fill() {
		entity = new Dwelling() ;
		
	}
	
	public void reverseFill() {
		
	}
	
	@Override
	public void windowClose(CloseEvent e) {
		getUi().mapV.removeFeatureFromLayer("SELCTION LAYER",drawnFeatureId) ;
	}
}