package com.nogroup.gov.gis.vui;

import javax.servlet.annotation.WebServlet;

import com.nogroup.gov.gis.vui.data.bundles.SessionLang;
import com.nogroup.gov.gis.vui.prez.home.HomeV;
import com.nogroup.gov.gis.vui.prez.home.MapV;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Push
@Theme("VTheme")
public class VUI extends UI {

    private static final int POLL_INTERVAL = 10000;
	public SessionLang lang = SessionLang.ENGLISH;
	public MapV mapV;

	@Override
    protected void init(VaadinRequest vaadinRequest) {
		setPollInterval(POLL_INTERVAL);
        setContent(new HomeV(this));
    }

    @WebServlet(urlPatterns = "/*", name = "VUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = VUI.class, productionMode = false)
    public static class VUIServlet extends VaadinServlet {
    }
}
