package com.nogroup.gov.gis.vui.prez.home;

public interface OnGeometryDrawnWindowAction {
	public void before(String data) ;
	public void after(String data) ;
}
