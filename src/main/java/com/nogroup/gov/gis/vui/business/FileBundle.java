package com.nogroup.gov.gis.vui.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

import com.nogroup.gov.gis.vui.data.embedded.VFile;

public class FileBundle {

	public static byte[] file2bytes(File file) throws IOException {
		byte[] bytesArray = new byte[(int) file.length()]; 
		
		FileInputStream fis = new FileInputStream(file);
		fis.read(bytesArray); //read file into bytes[]
		fis.close();
		
		return bytesArray ;
	}
	
	public static byte[] decode(String s) {
		return Base64.getDecoder().decode(s) ;
	}
	public static String encode(byte[] data) {
		return Base64.getEncoder().encodeToString(data);
	}
	
	public static VFile create(String name,String extension,File f) throws IOException {
		VFile vf= new VFile() ;
		vf.setContent(encode(file2bytes(f)));
		vf.setExtension(extension);
		vf.setName(name);
		return vf ;
	}
}
