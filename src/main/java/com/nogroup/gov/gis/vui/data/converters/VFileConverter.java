package com.nogroup.gov.gis.vui.data.converters;

import java.io.IOException;
import java.util.HashMap;

import javax.persistence.AttributeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.gov.gis.vui.data.embedded.VFile;

@SuppressWarnings("rawtypes")
public class VFileConverter implements AttributeConverter<VFile, String>{


    @Override
    public String convertToDatabaseColumn(VFile obj) {
        ObjectMapper om = new ObjectMapper();
        String op = null;
        try {
            op = om.writeValueAsString(obj);
            return (op);
        } catch (JsonProcessingException _x) {
        }
        return op ;
    }

    @Override
    public VFile convertToEntityAttribute(String val) {
        ObjectMapper om = new ObjectMapper();
        VFile op = null;
        try {
            op = om.readValue(val, VFile.class);
            return (op);
        } catch (IOException _x) {
        }
        return op ;
    }

}
