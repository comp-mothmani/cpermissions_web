package com.nogroup.gov.gis.vui.prez.ccmp.wndws;

import java.io.Serializable;
import java.util.List;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

public abstract class ExportWindow extends CWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	
	public CForm form = new CForm() ;
	public NativeButton exportBtn;
	public NativeButton closeBtn;

	public ExportWindow(){
		
	}
	
	public void initExportWindow(){
		setWidth("60%");
		setHeight("50%");
		init();
		addFooter();
		
		exportBtn = addButton("Export",new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				
			}
		}) ;
		
		closeBtn = addButton("Close",new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				ExportWindow.this.close();

			}
		}) ;
		
		setCaption(caption());
		addContent(form);
	}
	
	public abstract String caption() ;
	
	public class CForm extends VerticalLayout{

		public CForm(){
			
		}
	}
}
