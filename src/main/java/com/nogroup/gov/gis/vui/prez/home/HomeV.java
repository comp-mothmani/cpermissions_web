package com.nogroup.gov.gis.vui.prez.home;

import com.nogroup.gov.gis.vui.VUI;
import com.nogroup.gov.gis.vui.prez.modules.dwelling.CreateDwellingUnitV;
import com.nogroup.gov.gis.vui.prez.modules.municipality.MunicipalityV;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class HomeV extends VerticalLayout{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	private VUI ui;
	
	HorizontalLayout header = new HorizontalLayout() ;

	public HomeV(VUI ui) {
		this.ui = ui ;
		
		addStyleName("small-margins");
		setSpacing(false);
		setSizeFull();
		
		header.setWidth("100%");
		header.setSpacing(false);
		header.addStyleName(ValoTheme.LAYOUT_WELL);
		addComponent(header);
		
		MenuBar menu = new MenuBar() ;
		menu.setSizeFull();
		menu.addStyleName(ValoTheme.MENUBAR_SMALL);
		
		MenuItem mn_ = menu.addItem("Municipality") ;
		mn_.addItem("Settings", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				ui.addWindow(new MunicipalityV(ui));
				
			}
		}) ;
		mn_.addItem("New Subdivision") ;
		mn_.addItem("Manage Subdivision") ;
		mn_.addSeparator();
		mn_.addItem("Statistics") ;
		MenuItem mn_1 = menu.addItem("Dwelling") ;
		mn_1.addItem("New Dwelling Unit", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				ui.addWindow(new CreateDwellingUnitV(ui));
				
			}
		}) ;
		mn_1.addItem("Manage Units") ;
		mn_1.addItem("Search ...") ;
		MenuItem mn_2 = menu.addItem("Infraction") ;
		mn_2.addItem("New Infraction") ;
		mn_2.addItem("Manage Infractions") ;
		
		header.addComponent(menu);
		header.setExpandRatio(menu, 1);
		
		MenuBar menu2 = new MenuBar() ;
		menu2.setHeight("100%");
		menu2.addStyleName(ValoTheme.MENUBAR_SMALL);
		
		MenuItem mn = menu2.addItem("") ;
		mn.setIcon(FontAwesome.USER);
		mn.addItem("User Profile") ;
		mn.addItem("User Stories") ;
		
		MenuItem mn2 = menu2.addItem("")  ;
		mn2.setIcon(FontAwesome.ADJUST);
		mn2.addItem("General Settings") ;
		mn2.addItem("Server Settings") ;
		
		header.addComponent(menu2);
		
		MapV map = new MapV(ui) ;
		addComponent(map);
		setExpandRatio(map, 1);
	}

}
