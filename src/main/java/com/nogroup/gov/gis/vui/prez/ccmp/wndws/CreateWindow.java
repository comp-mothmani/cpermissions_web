package com.nogroup.gov.gis.vui.prez.ccmp.wndws;

import com.nogroup.gov.gis.vui.VUI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.NativeButton;

public abstract class CreateWindow extends CWindow{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	public NativeButton saveBtn;
	public NativeButton cancelBtn;
	protected VUI ui;
	
	public CreateWindow(VUI ui) {
		this.ui = ui ;
		initCreateWindow();
	}
	public void initCreateWindow(){
		setWidth("50%");
		setHeight("60%");
		init();
		addFooter();
		saveBtn = addButton("Save",new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				saveButtonCallBack() ;
			}
		}) ;
		cancelBtn = addButton("Close",new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cancelButtonCallBack() ;
			}
		}) ;
		
		setCaption(caption());
		addContent(content());
		setClosable(false);
	}
	public void saveButtonCallBack(){
		saveButtonClicked() ;
	}
	
	public VUI getUi() {
		return ui;
	}
	
	public abstract void saveButtonClicked() ;
	public abstract String caption() ;
	public abstract ComponentContainer content() ;
	public abstract void cancelButtonCallBack() ;

}
