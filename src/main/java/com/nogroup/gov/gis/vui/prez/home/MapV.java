package com.nogroup.gov.gis.vui.prez.home;

import org.vaadin.addon.vol3.OLMap;
import org.vaadin.addon.vol3.OLView;
import org.vaadin.addon.vol3.OLViewOptions;
import org.vaadin.addon.vol3.client.OLCoordinate;
import org.vaadin.addon.vol3.client.Projections;
import org.vaadin.addon.vol3.client.control.OLLayerSwitcherControl;
import org.vaadin.addon.vol3.interaction.OLDrawInteraction;
import org.vaadin.addon.vol3.interaction.OLDrawInteractionOptions.DrawingType;
import org.vaadin.addon.vol3.layer.OLLayer;
import org.vaadin.addon.vol3.layer.OLTileLayer;
import org.vaadin.addon.vol3.layer.OLVectorLayer;
import org.vaadin.addon.vol3.source.OLOSMSource;
import org.vaadin.addon.vol3.source.OLSource;
import org.vaadin.addon.vol3.source.OLVectorSource;

import com.nogroup.gov.gis.vui.VUI;
import com.nogroup.gov.gis.vui.data.embedded.VPoint;
import com.nogroup.gov.gis.vui.data.embedded.VPolygon;
import com.vaadin.ui.VerticalLayout;

public class MapV extends VerticalLayout{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	private OLMap map;
	private OLTileLayer osm;
	private VUI ui;

	private OLVectorLayer selectionLayer;
	private OLVectorSource selectionLayerSource;
	
	public MapV(VUI ui) {
		this.ui = ui ;
		this.ui.mapV = this ;
		
		setMargin(false);
		setSpacing(false);
		setSizeFull();
		
		map = new OLMap();
		map.setId("canvas");
		map.setView(createView());
		map.setSizeFull();
		addComponent(map);
		setExpandRatio(map, 1);
		map.setLayerSwitcherControl(new OLLayerSwitcherControl());
		
		osm = new OLTileLayer(createTileSource());
		osm.setTitle("OSM LAYER");
		osm.setLayerVisible(true);
		map.addLayer(osm);
		
		selectionLayerSource = new OLVectorSource() ;
		selectionLayer = new OLVectorLayer(selectionLayerSource) ;
		selectionLayer.setTitle("SELCTION LAYER");
		selectionLayer.setLayerVisible(true);
		map.addLayer(selectionLayer);
	}
	
	protected OLSource createTileSource() {		
		return new OLOSMSource();
	}
	
	protected OLView createView() {
		OLViewOptions options = new OLViewOptions();
		options.setInputProjection(Projections.EPSG4326);
		OLView view = new OLView(options);
		
		view.setCenter(new OLCoordinate(9.5375,33.8869));
		view.setZoom(7);
		return view;
	}
	
	public void drawPoint(OnVPointDrawn callback) {
		OLDrawInteraction interaction;
		map.addInteraction(interaction = new OLDrawInteraction(selectionLayer, DrawingType.POINT));
		selectionLayerSource.addFeatureSetChangeListener(new OnPointFeatureDrawn() {

			@Override
			public void feature(String id,VPoint feature) {
				map.removeInteraction(interaction);
				selectionLayerSource.removeFeatureById(id);
				callback.back(id,feature);
			}
			
		});
	}
	
	public void drawPoint(OnVPointDrawn callback, boolean removeOnClose) {
		OLDrawInteraction interaction;
		map.addInteraction(interaction = new OLDrawInteraction(selectionLayer, DrawingType.POINT));
		selectionLayerSource.addFeatureSetChangeListener(new OnPointFeatureDrawn() {

			@Override
			public void feature(String id,VPoint feature) {
				map.removeInteraction(interaction);
				if(!removeOnClose) {
					selectionLayerSource.removeFeatureById(id);
				}
				callback.back(id,feature);
			}
			
		});
		
	}
	public void drawPolygon(OnVPolygonDrawn callback, boolean removeOnClose) {
		OLDrawInteraction interaction;
		map.addInteraction(interaction = new OLDrawInteraction(selectionLayer, DrawingType.POLYGON));
		selectionLayerSource.addFeatureSetChangeListener(new OnPolygonFeatureDrawn() {

			@Override
			public void feature(String id,VPolygon feature) {
				map.removeInteraction(interaction);
				if(!removeOnClose) {
					selectionLayerSource.removeFeatureById(id);
				}
				callback.back(id,feature);
			}
			
		});
	}
	
	public void drawPolygon(OnVPolygonDrawn callback) {
		OLDrawInteraction interaction;
		map.addInteraction(interaction = new OLDrawInteraction(selectionLayer, DrawingType.POLYGON));
		selectionLayerSource.addFeatureSetChangeListener(new OnPolygonFeatureDrawn() {

			@Override
			public void feature(String id,VPolygon feature) {
				map.removeInteraction(interaction);
				selectionLayerSource.removeFeatureById(id);
				callback.back(id,feature);
			}
			
		});
	}
	public void center(double lat,double lon) {
		map.getView().setCenter(lon,lat);
	}
	protected void center(VPoint vp) {
		map.getView().setCenter(vp.getLongitude(), vp.getLatitude());
	}

	public void removeFeatureFromLayer(String name, String id) {
		OLVectorLayer layer = (OLVectorLayer) map.getLayers().stream()
		  .filter(lyr -> name.equals(lyr.getTitle()))
		  .findAny()
		  .orElse(null);
		
		if(layer != null) {
			OLVectorSource src = (OLVectorSource) layer.getSource() ;
			src.removeFeatureById(id);
		}
		
	}
}
