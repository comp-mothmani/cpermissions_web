package com.nogroup.gov.gis.vui.data.embedded;

import java.util.ArrayList;

public class VPolygon extends ArrayList<VPoint>{

	public VPoint getCentroid() {
		double lats = 0 ;
		double lons = 0 ;
		int d = 0 ;
		for(VPoint tmp0 : this) {
			d += 1 ;
			lats += tmp0.getLatitude() ;
			lons += tmp0.getLongitude() ;
		}
		VPoint vp = new VPoint(lons/d,lats/d,0) ;
		return vp ;
	}
	
	
}
