package com.nogroup.gov.gis.vui.prez.modules.municipality;

import com.nogroup.gov.gis.vui.VUI;
import com.nogroup.gov.gis.vui.data.daos.MunicipalityD;
import com.nogroup.gov.gis.vui.data.entities.Municipality;
import com.nogroup.gov.gis.vui.prez.ccmp.flds.MultiLangNameField;
import com.nogroup.gov.gis.vui.prez.ccmp.flds.SelectGeometryComponent;
import com.nogroup.gov.gis.vui.prez.ccmp.wndws.CreateWindow;
import com.nogroup.gov.gis.vui.prez.home.OnCissorsClicked;
import com.nogroup.gov.gis.vui.prez.home.OnGeometryDrawnWindowAction;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.ValoTheme;

public class MunicipalityV extends CreateWindow implements CloseListener{
	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	private SelectGeometryComponent geometryField;
	private MultiLangNameField tfName;
	private Municipality entity ;
	protected String drawnFeatureId;
	
	public MunicipalityV(VUI ui) {
		super(ui) ;
		addCloseListener(this);
	}
	
	@Override
	public void saveButtonClicked() {
		fill() ;
		new MunicipalityD().create(entity) ;
		MunicipalityV.this.close();
	}

	@Override
	public String caption() {
		return "Settings";
	}

	@Override
	public ComponentContainer content() {
		VerticalLayout vl = new VerticalLayout() ;
		vl.setWidth("100%");
		
		tfName = new MultiLangNameField("Name") ;
		tfName.setWidth("100%");
		tfName.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tfName);
		
		geometryField = new SelectGeometryComponent(getUi(), new OnGeometryDrawnWindowAction() {

			@Override
			public void before(String data) {
				getUi().mapV.removeFeatureFromLayer("SELCTION LAYER",drawnFeatureId) ;
				MunicipalityV.this.setVisible(false);
			}

			@Override
			public void after(String data) {
				drawnFeatureId = data ;
				MunicipalityV.this.setVisible(true);
			}
			
		},true);
		geometryField.setOnCissorsClicked(new OnCissorsClicked() {

			@Override
			public void action(String data) {
				getUi().mapV.removeFeatureFromLayer("SELCTION LAYER",drawnFeatureId) ;
			}
			
		});
		vl.addComponent(geometryField);
		
		if(new MunicipalityD().count() != 0) {
			entity = new MunicipalityD().read().get(0) ;
			reverseFill();
		}
		
		return vl ;
	}

	@Override
	public void cancelButtonCallBack() {
		MunicipalityV.this.close();
	}
	
	public void fill() {
		entity = new Municipality() ;
		entity.setCoordinates(geometryField.getValue());
		entity.setName(tfName.getValue());
	}
	
	public void reverseFill() {
		geometryField.setValue(entity.getCoordinates());
		tfName.setValue(entity.getName());
	}

	@Override
	public void windowClose(CloseEvent e) {
		getUi().mapV.removeFeatureFromLayer("SELCTION LAYER",drawnFeatureId) ;
	}
}
