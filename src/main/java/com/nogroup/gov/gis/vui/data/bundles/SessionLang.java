package com.nogroup.gov.gis.vui.data.bundles;

public enum SessionLang {
	ARABIC,
	ENGLISH,
	FRENCH
}
