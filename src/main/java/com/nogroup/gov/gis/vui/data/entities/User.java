package com.nogroup.gov.gis.vui.data.entities;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.nogroup.gov.gis.vui.data.converters.HashMapConverter;
import com.nogroup.gov.gis.vui.data.converters.MultiLangNameConverter;
import com.nogroup.gov.gis.vui.data.converters.ShortDateConverter;
import com.nogroup.gov.gis.vui.data.converters.VFileConverter;
import com.nogroup.gov.gis.vui.data.embedded.MultiLangName;
import com.nogroup.gov.gis.vui.data.embedded.VFile;

@Entity
@Table(name = "USER")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "name")
	@Convert(converter = MultiLangNameConverter.class)
    private MultiLangName name;
	
	@Column(name = "email")
    private String email;
	
	@Column(name = "hierarchy")
    private String hierarchy;
	
	@Column(name = "dCreated")
    @Convert(converter = ShortDateConverter.class)
    private Date dCreated;
	
	@Column(name = "picture",length=999999)
    @Convert(converter = VFileConverter.class)
    public VFile picture;
	
	@Column(name = "other",length=999999)
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MultiLangName getName() {
		return name;
	}

	public void setName(MultiLangName name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getdCreated() {
		return dCreated;
	}

	public void setdCreated(Date dCreated) {
		this.dCreated = dCreated;
	}

	public VFile getPicture() {
		return picture;
	}

	public void setPicture(VFile picture) {
		this.picture = picture;
	}

	public HashMap<String, Object> getOther() {
		return other;
	}

	public void setOther(HashMap<String, Object> other) {
		this.other = other;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}
	
}
